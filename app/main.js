'use strict';

const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
let mainWindow;


let menuTemplate = [
    {
        label: "File",
        submenu: [
            {
                label: "Exit",
                click () {
                    app.exit();
                }
            }
        ]
    }
];
const menu = electron.Menu.buildFromTemplate(menuTemplate);

function createWindow () {
   
    var electronScreen = electron.screen;
    var size = electronScreen.getPrimaryDisplay().workAreaSize;
    mainWindow = new BrowserWindow({
        width: size.width - 250, 
        height: size.height - 150,
        center: true,
        x: 100,
        y: 50,
        "node-integration": false
    });
    
    mainWindow.setMenu(menu);
    //mainWindow.webContents.openDevTools();

    mainWindow.loadURL('file://' + __dirname + '/snap/snap.html');

    mainWindow.on('close', () => {
        app.exit();
    });
    
    mainWindow.on('closed', () => {
        mainWindow = null;
    });

}

app.on('ready', createWindow);

/*
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (mainWindow === null) {
        createWindow();
    }
});
*/
