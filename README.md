Snap! local runner
================================================================================

**snap-package** prepare a Snap! simple distribution packages based on Electron.

**Depends on**: NodeJs and electron-packager

**Use**:

*snap-package master|release [snap version] [electron version]*

**USE AT OWN RISK!**    
script makes and deletes some build directories without interaction!
